package fun.bb1.spigot.itemframesnipper;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * Copyright 2023 BradBot_1
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class EntryPoint extends JavaPlugin implements Listener {
		
	@Override
	public void onEnable() {
		this.getLogger().info("Thank you for using ItemframeSnipper!");
		this.getServer().getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable() {
		PlayerInteractAtEntityEvent.getHandlerList().unregister((JavaPlugin)this);
	}
	
	@EventHandler
	public final void interactItemEventHandler(final PlayerInteractEntityEvent event) {
		if (!event.getPlayer().hasPermission("itemframe.snip")) return;
		if (event.getPlayer().isSneaking()) return;
		if (!(event.getRightClicked() instanceof ItemFrame itemFrame) || !itemFrame.isVisible()) return;
		final ItemStack itemStack = event.getPlayer().getInventory().getItem(event.getHand());
		if (itemStack == null || itemStack.getType() != Material.SHEARS || !(itemStack.getItemMeta() instanceof Damageable damageable)) return;
		if (!event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
			damageable.setDamage(damageable.getDamage() + 1);
			itemStack.setItemMeta(damageable);
		}
		itemFrame.setVisible(false);
		event.setCancelled(true);
	}
	
}
