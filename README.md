# ItemframeSnipper

A Minecraft plugin to allow itemframes to be sheared

> *Reload ready! This plugin has no adverse effects when /reload is executed: commands will be disabled and added according to the changes to the configuration*

## Permissions

The permission `itemframe.snip` is checked before shearing the itemframe, this is provided to all players by default.

## Images

![Itemframes invisible and not](https://i.ibb.co/W2qfXBP/image.png)